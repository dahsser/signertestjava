import com.java4less.pdf.signature.PDFSignatureFacade;

public class SignatureTest {

	public static void main(String[] args) {
		PDFSignatureFacade facade = new PDFSignatureFacade();
		
		if (args.length!=4) System.err.println("Usage: p12file password inputfile outputfile");
		
		try {		
			facade.sign(args[0],args[1],args[2],args[3],true,null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
