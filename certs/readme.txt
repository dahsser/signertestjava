This directory contains the certificate files you can use to test the PDF signature feature
- the file ca.cer is the certificate of a Certification Authority (CA) used in the tests
- the file j4l_test.p12 contains a test certificate (issued by the CA) and private key you can use to sign PDF files. The password of this file is "test" (without quotes)