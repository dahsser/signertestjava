
FILES:

- runTest.bat: runs the test class that will sign the file report.pdf
- Javadoc\ directory: javadoc files.
- help: help/tutorial
- lib directory: thrid party libraries
- j4lpdfsign.jar: PDF signature classes



QUESTIONS, COMMENTS ...

In case of questions, comments... you can contact the author to Java4Less@confluencia.net


REGISTRATION


If you are interested in the product, you can contact us by e-mail to the address Java4Less@confluencia.net.

http://www.java4less.com



--------------------------------------------------------------------------------------
EVALUATION LICENSE AGREEMENT (this does not apply if you already bought the full version)


IMPORTANT - READ CAREFULLY

This license statement constitutes a legal agreement between you (either as an individual or a single entity) and J4L Components (http://www.java4less.com) for the evaluation version of the product including any examples, source code and documentation.

BY DECOMPRESSING, COPYING, OR OTHERWISE USING THE RECEIVED ZIP FILES, YOU AGREE TO BE BOUND BY ALL OF THE TERMS AND CONDITIONS OF THE LICENSE AGREEMENT.

Upon your acceptance of the terms and conditions of the License Agreement, you are granted the use of the Software in the manner provided below.

This Software is protected by copyright law and international copyright treaty. Therefore, you must treat this Software like any other copyrighted material (e.g., a book). 


EVALUATION LICENSE TERMS


1. The evaluation version of the product can only be used for evaluation purposes. 
2. You may not include any component of this version in any final product or application.
3. You may not redistribute any components of this product.
4. You must delete this software after 30 days if you have not purchased the full version.
5. Reverse engineering, reverse compiling, or disassembly of the Software is expressly prohibited.


TRADE SECRETS


You acknowledge and agree that the structure, sequence and organization of the Software are the valuable trade secrets of J4L Components (http://www.java4less.com). You agree to hold such trade secrets in confidence. 


NO WARRANTY: THE SOFTWARE AND DOCUMENTATION ARE PROVIDED ON AN "AS IS" BASIS AND ALL RISK IS WITH YOU. J4L COMPONENTS (http://www.java4less.com) MAKES NO REPRESENTATIONS OR WARRANTIES THAT THE SOFTWARE AND DOCUMENTATION PROVIDED ARE FREE OF ERRORS OR VIRUSES OR THAT THE SOFTWARE AND DOCUMENTATION ARE SUITABLE FOR YOUR INTENDED USE. 

LIMITATION OF LIABILITY: IN NO EVENT SHALL J4L COMPONENTS (http://www.java4less.com) OR ITS SUPPLIERS BE LIABLE TO YOU OR ANY OTHER PARTY FOR ANY INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES, LOSS OF DATA OR DATA BEING RENDERED INACCURATE, LOSS OF PROFITS OR REVENUE, OR INTERRUPTION OF BUSINESS IN ANY WAY ARISING OUT OF OR RELATED TO THE USE OR INABILITY TO USE THE SOFTWARE AND/OR DOCUMENTATION, REGARDLESS OF 
THE FORM OF ACTION, WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT  PRODUCT LIABILITY OR OTHERWISE, EVEN IF ANY REPRESENTATIVE OF J4L COMPONENTS OR ITS SUPPLIERS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THIS  DISCLAIMER OF LIABILITY FOR DAMAGES WILL NOT BE AFFECTED BY ANY FAILURE OF  THE SOLE AND EXCLUSIVE REMEDIES HEREUNDER.

 




